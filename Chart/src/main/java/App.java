import java.util.Date;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.BurlapProxyFactoryBean;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;

@SuppressWarnings("deprecation")
@Configuration
public class App {

	 @Bean
	    public HttpInvokerProxyFactoryBean invoker() {
	        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
	        invoker.setServiceUrl("http://localhost:8080/api");
	        invoker.setServiceInterface(ServerAPI.class);
	        return invoker;
	    }
	
	public static void main(String[] args) {
		SpringApplicationBuilder builder = new SpringApplicationBuilder(App.class);
		builder.headless(false);
		
		ServerAPI service = builder
		          .run(args)
		          .getBean(ServerAPI.class);
		
		View view = new View();
        Controller controller = new Controller(view, service);
        
        view.setVisible(true);
	}

}
