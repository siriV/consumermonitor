import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;

import javax.swing.JOptionPane;
import javax.xml.crypto.Data;

public class Controller {

	private View view;
	private ServerAPI server;
	public Controller(View view, ServerAPI server)
	{
		this.view = view;
		this.server = server;
		this.view.addButtonListener(new userListener(this.view, this.server));
		this.view.addConsumerListener(new durationListener(this.view, this.server));

	}
	class userListener implements ActionListener
	{
		private View view;
		private ServerAPI server;
		
		public userListener(View view, ServerAPI server)
		{
			this.view = view;
			this.server = server;
		}
		
		public void actionPerformed(ActionEvent e)
		{
			Long userId = view.getUserId();
			Map<Date, Double> data = null;
			try {
				data = server.getSensorData(userId);
				view.displayGraph(data);


			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, ex.getMessage());

			}
			
		}
	}
	
	class durationListener implements ActionListener
	{
		private View view;
		private ServerAPI server;
		
		public durationListener(View view, ServerAPI server)
		{
			this.view = view;
			this.server = server;
		}
		
		public void actionPerformed(ActionEvent e)
		{
			Long userId = view.getUserId();
			Integer duration = view.getDuration();
			Integer nrOfDays = view.getNrOfDays();
			Map<Date, Double> data = null;
			Double minConsumption = 10000.0;
			Integer minIndex = 0;
			try {
				data = new LinkedHashMap(server.getSensorData(userId));
				List<Double> values = new ArrayList<Double>(data.values());
				Integer index = 0;
				for(int i = 0; i < nrOfDays - duration; i++)
				{
					Double cons = 0.0;
					for(int j = i; j < i + duration; j++)
					{
						cons += values.get(i);
					}
					
					if(cons < minConsumption)
					{
						cons = minConsumption;
						minIndex = i;
					}
					
				}
				Date minDate = new ArrayList<Date>(data.keySet()).get(minIndex);
				minDate = minDate.from(minDate.toInstant());
				Date maxDate = new ArrayList<Date>(data.keySet()).get(minIndex + duration);
				maxDate = maxDate.from(maxDate.toInstant());
				view.displayConsumption(minDate, maxDate);

			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, ex.getMessage());

			}
		}
	}

}
