import java.util.Date;
import java.util.Map;

public interface ServerAPI {

	public Map<Date, Double> getSensorData(Long userId);
}
