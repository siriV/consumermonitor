import java.awt.Dimension;
import java.awt.Panel;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Map;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

public class View extends JFrame{

	JLabel userIdLabel = new JLabel("UserId: ");
	JTextField userIdText = new JTextField();
	
	JLabel nrOfDaysLabel = new JLabel("Number of days: ");
	JTextField nrOfDaysText = new JTextField();
	JButton getDataButton = new JButton("Get data");
	
	JLabel durationLabel = new JLabel("Duration: ");
	JTextField durationText = new JTextField();
	JButton durationButton = new JButton("Dutation: ");
	
	JPanel panel = new JPanel();
	JFreeChart chart;
	Double mean;
	Integer nrOfDays;
	
	public View()
	{
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(850, 800);
		this.setResizable(false);
		
		addPannels();
		panel.add(Box.createRigidArea(new Dimension(0, 750)));
		this.add(panel);

	}
	
	private void addPannels()
	{
		JPanel userIdPanel = new JPanel();
		JPanel durationPanel = new JPanel();

		userIdText.setSize(20,60);
		userIdPanel.setLayout(new BoxLayout(userIdPanel, BoxLayout.X_AXIS));
		userIdPanel.add(userIdLabel);
		userIdPanel.add(Box.createRigidArea(new Dimension(25, 0)));
		userIdPanel.add(userIdText);
		userIdPanel.add(Box.createRigidArea(new Dimension(25, 0)));
		userIdPanel.add(nrOfDaysLabel);
		userIdPanel.add(Box.createRigidArea(new Dimension(25, 0)));
		userIdPanel.add(nrOfDaysText);
		userIdPanel.add(Box.createRigidArea(new Dimension(25, 0)));
		userIdPanel.add(getDataButton);
		userIdPanel.add(Box.createRigidArea(new Dimension(25, 0)));

		durationText.setSize(20,60);
		durationPanel.setLayout(new BoxLayout(durationPanel, BoxLayout.X_AXIS));
		durationPanel.add(durationLabel);
		durationPanel.add(Box.createRigidArea(new Dimension(25, 0)));
		durationPanel.add(durationText);
		durationPanel.add(Box.createRigidArea(new Dimension(25, 0)));
		durationPanel.add(durationButton);

		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(Box.createRigidArea(new Dimension(0, 30)));
		panel.add(userIdPanel);
		panel.add(Box.createRigidArea(new Dimension(0, 30)));
		panel.add(durationPanel);
		
	}
	
	public Long getUserId()
	{
		String userIdText = new String(this.userIdText.getText());
		Long userId = Long.parseLong(userIdText);
		return userId;

	}
	
	public Integer getDuration()
	{
		String durationText = new String(this.durationText.getText());
		Integer duration = Integer.parseInt(durationText);
		return duration;

	}
	
	public Integer getNrOfDays()
	{
		return nrOfDays;

	}
	
	public void addButtonListener(ActionListener listen)
	{
		getDataButton.addActionListener(listen);
	}
	
	public void addConsumerListener(ActionListener listen)
	{
		durationButton.addActionListener(listen);
	}
	
	public void displayGraph(Map<Date, Double> data)
	{
		String chartTitle = "Daily energy consumption";
	    String categoryAxisLabel = "Date";
	    String valueAxisLabel = "Consumption";
	    mean = 0.0;

	    CategoryDataset dataset = new DefaultCategoryDataset();
	    nrOfDays = Math.min(Integer.parseInt(nrOfDaysText.getText()), data.size());
	    Integer index = 0;
	    for(Map.Entry<Date, Double> d : data.entrySet()){
	    	Date dateDisplay = d.getKey().from(d.getKey().toInstant());
	        ((DefaultCategoryDataset) dataset).addValue(d.getValue(), "consumption", dateDisplay);
	        mean += d.getValue();
	        index++;
	        if(nrOfDays == index)
	        	break;
	    }
	    mean /= nrOfDays;
	    
	    chart = ChartFactory.createLineChart(chartTitle,
	            categoryAxisLabel, valueAxisLabel, dataset, PlotOrientation.VERTICAL, rootPaneCheckingEnabled, rootPaneCheckingEnabled, rootPaneCheckingEnabled);
	    CategoryPlot plot = chart.getCategoryPlot();
	    LineAndShapeRenderer renderer = new LineAndShapeRenderer();
	    plot.setRenderer(renderer);
	    ChartPanel chartPanel =  new ChartPanel(chart);
	    
	    panel.removeAll();
	    panel.updateUI();
		addPannels();
		panel.add(Box.createRigidArea(new Dimension(0, 50)));
		panel.add(chartPanel);
		panel.add(Box.createRigidArea(new Dimension(0, 50)));
		
		JLabel meanLabel = new JLabel("Mean consumption: " + mean);
		panel.add(meanLabel);
	    this.add(panel);
	}
	
	public void displayConsumption(Date minHour, Date maxHour)
	{
		panel.removeAll();
	    panel.updateUI();
		addPannels();
		panel.add(Box.createRigidArea(new Dimension(0, 50)));
		ChartPanel chartPanel =  new ChartPanel(chart);
		panel.add(chartPanel);
		panel.add(Box.createRigidArea(new Dimension(0, 50)));
		
		JLabel consumptionLabel = new JLabel("Plug device from " + minHour + " to " + maxHour);
		panel.add(consumptionLabel);
		
	    this.add(panel);
	}

}
