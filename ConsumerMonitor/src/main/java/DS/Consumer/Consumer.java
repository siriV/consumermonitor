package DS.Consumer;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import DS.Sensor.Sensor;
import DS.SensorData.SensorData;
import DS.User.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
@Setter
@Getter
public class Consumer implements Serializable{

    private static final long serialVersionUID = 1L;
    
    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
    @Column(name = "description", nullable = false)
	private String description;
	
	@Column(name = "address", nullable = false)
	private String address;
	
	@Column(name = "max_consumption", nullable = false)
	private Double maxConsumption;
	
	@Column(name = "avg_consumption", nullable = false)
	private Double avgConsumption;
	
	@OneToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "sensor_id", referencedColumnName = "id")
	private Sensor sensor;
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = true)
	private User user;
	
	@OneToMany(cascade = CascadeType.PERSIST, mappedBy = "consumer", orphanRemoval = true)
	private final Set<SensorData> sensorData = new HashSet<SensorData>();

}
