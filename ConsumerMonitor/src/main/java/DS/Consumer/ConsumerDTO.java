package DS.Consumer;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ConsumerDTO {

	private Long id;
	private Long userId;
	private String description;
	private String address;
	private Double maxConsumption;
	private Double avgConsumption;
}
