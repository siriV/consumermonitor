package DS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CosumerMonitorApplication{

	public static void main(String[] args) {
		SpringApplication.run(CosumerMonitorApplication.class, args);
	}

}
