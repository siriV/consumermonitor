package DS.Sensor;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class SensorDTO {

	private Long id;
	private String description;
	private Double maxValue;
}
