package DS;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestParam;

import com.googlecode.jsonrpc4j.JsonRpcParam;
import com.googlecode.jsonrpc4j.JsonRpcService;
import DS.SensorData.SensorDataDTO;


public interface ServerAPI {

	public Map<Date, Double> getSensorData(Long userId);
}
