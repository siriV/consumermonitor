package DS;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;

import DS.SensorData.SensorData;
import DS.SensorData.SensorDataDTO;
import DS.User.User;
import DS.User.UserRepository;
import DS.User.UserService;
import DS.User.UserServiceInterface;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Service
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ServerAPIImpl implements ServerAPI{
	
	
	private UserRepository uRepository;
	@Override
	public Map<Date, Double> getSensorData(Long userId)
	{
		User user = uRepository.findById(userId).orElseThrow(() -> new NoSuchElementException("Element does not exists"));
		
		Map<Date, Double> totalConsumption = user.getConsumers().stream()
								.map(c -> c.getSensorData())
								.flatMap(Collection::stream)
								.collect(Collectors.toList()).stream()
								.collect(Collectors.groupingBy(SensorData::getDate, Collectors.summingDouble(SensorData::getConsumption)));
		totalConsumption = new TreeMap<Date, Double>(totalConsumption);
		
		return totalConsumption;
	}
}
