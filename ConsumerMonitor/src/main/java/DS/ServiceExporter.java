package DS;

import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;

import DS.User.User;
import DS.User.UserRepository;
import lombok.Builder;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@Builder
public class ServiceExporter {

	private final UserRepository uRepository;
	@Bean(name = "/api") 
	HttpInvokerServiceExporter accountService() {
		HttpInvokerServiceExporter exporter = new HttpInvokerServiceExporter();
	    exporter.setService(new ServerAPIImpl(uRepository));
	    exporter.setServiceInterface( ServerAPI.class );
	    return exporter;
    }
}
