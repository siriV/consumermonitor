package DS.User;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LoginDTO {

	private Long id;
	private Role role;
}
