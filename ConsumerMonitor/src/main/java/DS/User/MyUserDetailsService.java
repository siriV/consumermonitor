package DS.User;

import java.util.NoSuchElementException;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class MyUserDetailsService implements UserDetailsService {

	private UserService uService;
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		
		try {
			User user = uService.getUser(email);
			return new MyUserDetails(user);
		} catch (NoSuchElementException e) {
			throw new UsernameNotFoundException("Not found!");
		}
	}

}
