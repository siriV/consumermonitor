package DS.User;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;

import DS.SensorData.SensorDataDTO;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
@CrossOrigin("https://energy-monitor.herokuapp.com")
public class RabbitMQConsumer {
	
	private final UserServiceInterface uService;
	private static Map<Long, Double> lastConsumption = new HashMap<Long, Double>();
	private final SimpMessagingTemplate smTemplate;

	@RabbitListener(queues = "MessageBroker")
	public void receiveMessage(String value)
	{
		try {
			String[] data = value.split("-");
			
			Long sensorId = Long.parseLong(data[0]);
			Long consumerId = Long.parseLong(data[1]);
	        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss zzz yyyy");
			LocalDateTime localDate = LocalDateTime.parse(data[2], dateFormat);
			Date date = java.sql.Timestamp.valueOf(localDate);
			Double record = Double.parseDouble(data[3]);
			SensorDataDTO sData = SensorDataDTO.builder()
												.consumerId(consumerId)
												.sensorId(sensorId)
												.date(date)
												.consumption(record)
												.build();
			if(this.lastConsumption.keySet().contains(consumerId))
			{
				double lastConsumption = this.lastConsumption.get(consumerId);
				Double peak = Math.abs(sData.getConsumption() - lastConsumption);
				Double maxValue = uService.getSensor(sData.getSensorId()).getMaxValue();
				
				if(peak > maxValue)
				{
					System.out.println("Sensor " + sData.getSensorId() + " had a peak: " + peak + " but it's max value is: " + maxValue);
					this.smTemplate.convertAndSend("/topic/socket/peak/" + consumerId, "Sensor " + sensorId + " had a peak: " + peak + " but it's max value is: " + maxValue);
					return;
				}
			}
			this.lastConsumption.put(consumerId, record);
			uService.addSensorData(sData);
			
			System.out.println(sData);
		
		}
		catch(Exception ex)
		{
			System.out.println("Invalid data");
		}
		
	}
}
