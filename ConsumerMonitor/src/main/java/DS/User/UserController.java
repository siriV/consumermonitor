package DS.User;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import DS.Consumer.Consumer;
import DS.Consumer.ConsumerDTO;
import DS.Sensor.Sensor;
import DS.Sensor.SensorDTO;
import DS.SensorData.SensorData;
import DS.SensorData.SensorDataDTO;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@CrossOrigin("http://localhost:8081")
public class UserController {

	private final UserServiceInterface uService;
	
	@GetMapping("/login/{email}/{pass}")
	public ResponseEntity<LoginDTO> checkCredentials(@PathVariable (name = "email") String email, @PathVariable (name = "pass") String password)
	{
	
		try
		{
			User databaseUser = uService.checkCredentials(email, password);
			LoginDTO data = LoginDTO.builder()
								.id(databaseUser.getId())
								.role(databaseUser.getRole())
								.build();
			
			return new ResponseEntity<LoginDTO>(data, HttpStatus.OK);
		}
		catch(Exception ex) {
			return new ResponseEntity<LoginDTO>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping("/register")
	public ResponseEntity<Long> createUser(@RequestBody User user)
	{
		try
		{
			User savedUser = uService.createUser(user);
			return new ResponseEntity<Long>(savedUser.getId(), HttpStatus.CREATED);
		}
		catch(Exception ex) {
			return new ResponseEntity<Long>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/new/consumer")
	public ResponseEntity<Long> createConsumer(@RequestBody Consumer consumer)
	{
		try
		{
			Consumer savedConsumer = uService.createConsumer(consumer);
			return new ResponseEntity<Long>(savedConsumer.getId(), HttpStatus.CREATED);
		}
		catch(Exception ex) {
			return new ResponseEntity<Long>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/new/sensor")
	public ResponseEntity<Long> createSensor(@RequestBody Sensor sensor)
	{
		try
		{
			Sensor savedSensor = uService.createSensor(sensor);
			return new ResponseEntity<Long>(savedSensor.getId(), HttpStatus.CREATED);
		}
		catch(Exception ex) {
			return new ResponseEntity<Long>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/new/sensorData")
	public ResponseEntity<Object> createSensorData(@RequestBody SensorDataDTO sensorData)
	{
		try
		{
			uService.addSensorData(sensorData);
			return new ResponseEntity( HttpStatus.CREATED);
		}
		catch(Exception ex) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping("/delete/user/{id}")
	public ResponseEntity<Object> deleteUser(@PathVariable(name = "id") Long id)
	{
		try {
			uService.deleteUser(id);
			return new ResponseEntity( HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping("/delete/consumer/{id}")
	public ResponseEntity<Object> deleteConsumer(@PathVariable(name = "id") Long id)
	{
		try {
			uService.deleteConsumer(id);
			return new ResponseEntity( HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping("/delete/sensor/{id}")
	public ResponseEntity<Object> deleteSensor(@PathVariable(name = "id") Long id)
	{
		try {
			uService.deleteSensor(id);
			return new ResponseEntity( HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
	}
	
	
	@PostMapping("/attach/C2U/{userId}/{consumerId}")
	public ResponseEntity<Object> attachConsumerToUser(@PathVariable(name = "userId") Long userId, @PathVariable(name = "consumerId") Long consumerId)
	{
		try {
			uService.attachConsumerToUser(userId, consumerId);
			return new ResponseEntity(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/attach/S2C/{consumerId}/{sensorId}")
	public ResponseEntity<Object> attachSensorToConsumer(@PathVariable(name = "consumerId") Long consumerId, @PathVariable(name = "sensorId") Long sensorId)
	{
		try {
			uService.attachSensorToConsumer(consumerId, sensorId);
			return new ResponseEntity(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/attach/SD2C")
	public ResponseEntity<Object> attachSensorDataToConsumer(@RequestBody SensorDataDTO sd)
	{
		try {
			uService.attachSensorDataToConsumer(sd);
			return new ResponseEntity(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/consumers/{userId}")
	public ResponseEntity<Set<ConsumerDTO>> getConsumersForUser(@PathVariable(name = "userId") Long userId)
	{
		try {
			Set<ConsumerDTO> data = uService.getAllConsumers(userId);
			return new ResponseEntity<Set<ConsumerDTO>>(data, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/data/{userId}/{consumerId}")
	public ResponseEntity<Double> getConsumptionForConsumer(@PathVariable(name = "userId") Long userId, @PathVariable(name = "consumerId") Long consumerId)
	{
		try {
			Double data = uService.getAllDataForUser(userId, consumerId);
			return new ResponseEntity<Double>(data, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/all_users")
	public ResponseEntity<Set<UserDTO>> getAllUsers()
	{
		try {
			Set<UserDTO> data = uService.getAllUsers();
			return new ResponseEntity<Set<UserDTO>>(data, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/all_consumers")
	public ResponseEntity<Set<ConsumerDTO>> getAllConsumers()
	{
		try {
			Set<ConsumerDTO> data = uService.getAllConsumers();
			return new ResponseEntity<Set<ConsumerDTO>>(data, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/all_sensors")
	public ResponseEntity<Set<SensorDTO>> getAllSensors()
	{
		try {
			Set<SensorDTO> data = uService.getAllSensors();
			return new ResponseEntity<Set<SensorDTO>>(data, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/consumption/{userId}")
	public ResponseEntity<Map<Date, Double>> getConsumptionForConsumer(@PathVariable(name = "userId") Long userId)
	{
		try {
			Map<Date, Double> data = uService.getTotalConsumption(userId);
			return new ResponseEntity<Map<Date, Double>>(data, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
	}
}
