package DS.User;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import DS.Consumer.Consumer;
import DS.Consumer.ConsumerDTO;
import DS.Consumer.ConsumerRepository;
import DS.Sensor.Sensor;
import DS.Sensor.SensorDTO;
import DS.Sensor.SensorRepository;
import DS.SensorData.SensorData;
import DS.SensorData.SensorDataDTO;
import DS.SensorData.SensorDataRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserService implements UserServiceInterface{

	private final UserRepository uRepository;
	private final PasswordEncoder encoder;
	private final ConsumerRepository cRepository;
	private final SensorRepository sRepository;
	private final SensorDataRepository sdRepository;
	
	public User checkCredentials(String email, String password) throws NoSuchElementException
	{
		
		User databaseUser = getUser(email);
		if(databaseUser.getEmail().equals(email) && encoder.matches(password, databaseUser.getPassword()))
		{
			return databaseUser;
		}
		else {
			throw new NoSuchElementException();
		}
	}
	
	public User createUser(User user)
	{
		user.setPassword(encoder.encode(user.getPassword()));
		User savedUser = uRepository.save(user);
		return savedUser;
	}
	
	public Consumer createConsumer(Consumer consumer)
	{
		Consumer savedConsumer = cRepository.save(consumer);
		return savedConsumer;
	}
	
	public Sensor createSensor(Sensor sensor)
	{
		Sensor savedSensor = sRepository.save(sensor);
		return savedSensor;
	}
	
	public void addSensorData(SensorDataDTO sd) throws NoSuchElementException
	{
		Consumer consumer = cRepository.findById(sd.getConsumerId()).orElseThrow(() -> new NoSuchElementException("Element does not exists"));
		Sensor sensor = consumer.getSensor();
		if(sensor == null) throw new NoSuchElementException();
		
		SensorData s = SensorData.builder()
									.consumer(consumer)
									.sensor(sensor)
									.consumption(sd.getConsumption())
									.date(sd.getDate())
									.build();
		
		sdRepository.save(s);
	}
	
	public void deleteUser(Long userId) throws NoSuchElementException
	{
		User user = uRepository.findById(userId).orElseThrow(() -> new NoSuchElementException("Element does not exists"));
		Set<Consumer> modifiedConsumers = new HashSet<Consumer>();
		
		for(Consumer c: user.getConsumers())
		{
			c.setUser(null);
			modifiedConsumers.add(c);
		}
		cRepository.saveAll(modifiedConsumers);
		uRepository.deleteById(userId);
	}
	
	public void deleteConsumer(Long consumerId) throws IllegalArgumentException, NoSuchElementException
	{
		Consumer consumer = cRepository.findById(consumerId).orElseThrow(() -> new NoSuchElementException("Element does not exists"));
		
		if(consumer.getUser() != null)
		{
			User user = consumer.getUser();
			user.getConsumers().remove(consumer);
			consumer.setUser(null);
		}
		cRepository.deleteById(consumerId);
	}
	
	public void deleteSensor(Long sensorId) throws IllegalArgumentException, NoSuchElementException
	{
		Sensor sensor = sRepository.findById(sensorId).orElseThrow(() -> new NoSuchElementException("Element does not exists"));
		
		if(sensor.getConsumer() != null)
		{
			Consumer consumer = sensor.getConsumer();
			consumer.setSensor(null);
			cRepository.save(consumer);
		}
		
		sRepository.deleteById(sensorId);
	}
	
	public void attachConsumerToUser(Long userId, Long consumerId) throws NoSuchElementException, IllegalArgumentException
	{
		User user = uRepository.findById(userId).orElseThrow(() -> new NoSuchElementException("Element does not exists"));
		Consumer consumer = cRepository.findById(consumerId).orElseThrow(() -> new NoSuchElementException("Element does not exists"));
		
		if(consumer.getUser() == null && user.getConsumers().add(consumer))
		{
			consumer.setUser(user);
			cRepository.save(consumer);
		}
		else {
			throw new IllegalArgumentException("Could not map user to consumer");
		}
	}
	
	
	public void attachSensorToConsumer(Long consumerId, Long sensorId) throws IllegalArgumentException, NoSuchElementException
	{
		Consumer consumer = cRepository.findById(consumerId).orElseThrow(() -> new NoSuchElementException("Element does not exists"));
		Sensor sensor = sRepository.findById(sensorId).orElseThrow(() -> new NoSuchElementException("Element does not exists"));
		
		if(consumer.getSensor() == null)
		{
			consumer.setSensor(sensor);
			sensor.setConsumer(consumer);
			cRepository.save(consumer);
		}
		else
		{
			throw new IllegalArgumentException("Could not map consumer to sensor");
		}
		
	}
	
	public void attachSensorDataToConsumer(SensorDataDTO sd) throws IllegalArgumentException, NoSuchElementException
	{
		Consumer consumer = cRepository.findById(sd.getConsumerId()).orElseThrow(() -> new NoSuchElementException("Element does not exists"));
		Sensor sensor = sRepository.findById(sd.getSensorId()).orElseThrow(() -> new NoSuchElementException("Element does not exists"));
		
		if(consumer.getSensor() == sensor && consumer.getUser() != null && sensor.getConsumer() == consumer)
		{
			SensorData sdData = SensorData.builder()
					.consumer(consumer)
					.sensor(sensor)
					.consumption(sd.getConsumption())
					.date(sd.getDate())
					.build();
			sdRepository.save(sdData);
		}
		else {
			throw new IllegalArgumentException("Sensor and Consumer are not linked");
		}
	}
	
	public User getUser(Long userId) throws NoSuchElementException
	{
		User user = uRepository.findById(userId).orElseThrow(() -> new NoSuchElementException("Element does not exists"));
		return user;
	} 
	
	public User getUser(String userEmail) throws NoSuchElementException
	{
		User user = uRepository.findByEmail(userEmail).orElseThrow(() -> new NoSuchElementException("Element does not exists"));
		return user;
	} 
	
	public Sensor getSensor(Long sensorId)
	{
		Sensor sensor = sRepository.findById(sensorId).orElseThrow(() -> new NoSuchElementException("Element does not exists"));
		return sensor;
	}

	public Set<ConsumerDTO> getAllConsumers(Long userId) throws NoSuchElementException
	{
		User user = uRepository.findById(userId).orElseThrow(() -> new NoSuchElementException("Element does not exists"));
		Set<ConsumerDTO> data = user.getConsumers().stream()
										.map(c -> ConsumerDTO.builder()
															 .id(c.getId())
															 .description(c.getDescription())
															 .maxConsumption(c.getMaxConsumption())
															 .avgConsumption(c.getAvgConsumption())
															 .address(c.getAddress())
															 .build())
										.collect(Collectors.toSet());
		return data;
	}
	
	public Double getAllDataForUser(Long userId, Long consumerId) throws NoSuchElementException
	{
		User user = uRepository.findById(userId).orElseThrow(() -> new NoSuchElementException("Element does not exists"));
		Consumer consumer = cRepository.findById(consumerId).orElseThrow(() -> new NoSuchElementException("Element does not exists"));
		if(!user.getConsumers().contains(consumer))
		{
			throw new NoSuchElementException();
		}
		Double data = consumer.getSensorData().stream()
													.mapToDouble(sd -> sd.getConsumption())
													.sum();
		
		
		return data;
	}
	
	public Map<Date, Double> getTotalConsumption(Long userId) throws NoSuchElementException
	{
		User user = uRepository.findById(userId).orElseThrow(() -> new NoSuchElementException("Element does not exists"));
	
		Map<Date, Double> totalConsumption = user.getConsumers().stream()
								.map(c -> c.getSensorData())
								.flatMap(Collection::stream)
								.collect(Collectors.toList()).stream()
								.collect(Collectors.groupingBy(SensorData::getDate, Collectors.summingDouble(SensorData::getConsumption)));
		
		return totalConsumption;
										
							
	}
	
	public Set<UserDTO> getAllUsers()
	{
		List<User> users = uRepository.findAll();
		Set<UserDTO> uDTO = users.stream()
									.filter(u -> u.getRole() == Role.USER)
									.map(u -> UserDTO.builder()
													.id(u.getId())
													.name(u.getName())
													.email(u.getEmail())
													.build())
									.collect(Collectors.toSet());
		return uDTO;
	}
	
	public Set<ConsumerDTO> getAllConsumers()
	{
		List<Consumer> consumers = cRepository.findAll();
		Set<ConsumerDTO> cDTO = consumers.stream()
									.map(c -> ConsumerDTO.builder()
													.id(c.getId())
													.description(c.getDescription())
													.maxConsumption(c.getMaxConsumption())
													.avgConsumption(c.getAvgConsumption())
													.address(c.getAddress())
													.build())
									.collect(Collectors.toSet());
		return cDTO;
	}
	
	public Set<SensorDTO> getAllSensors()
	{
		List<Sensor> sensor = sRepository.findAll();
		Set<SensorDTO> sDTO = sensor.stream()
									.map(s -> SensorDTO.builder()
													.id(s.getId())
													.description(s.getDescription())
													.maxValue(s.getMaxValue())
													.build())
									.collect(Collectors.toSet());
		return sDTO;
	}
	
}
