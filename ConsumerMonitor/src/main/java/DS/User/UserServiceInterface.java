package DS.User;


import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import DS.Consumer.Consumer;
import DS.Consumer.ConsumerDTO;
import DS.Sensor.Sensor;
import DS.Sensor.SensorDTO;
import DS.SensorData.SensorData;
import DS.SensorData.SensorDataDTO;

public interface UserServiceInterface {
	
	public User checkCredentials(String email, String password);
	public User createUser(User user);
	public Consumer createConsumer(Consumer consumer);
	public Sensor createSensor(Sensor sensor);
	public void addSensorData(SensorDataDTO sensorData);
	public void deleteUser(Long userId);
	public void deleteConsumer(Long consumerId);
	public void deleteSensor(Long sensorId);
	public User getUser(Long userId);
	public Sensor getSensor(Long sensorId);
	public User getUser(String userEmail);
	public void attachConsumerToUser(Long userId, Long consumerId);
	public void attachSensorToConsumer(Long consumerId, Long sensorId);
	public void attachSensorDataToConsumer(SensorDataDTO sd);
	public Set<ConsumerDTO> getAllConsumers(Long userId);
	public Set<ConsumerDTO> getAllConsumers();
	public Set<UserDTO> getAllUsers();
	public Double getAllDataForUser(Long userId, Long consumerId);
	public Set<SensorDTO> getAllSensors();
	public Map<Date, Double> getTotalConsumption(Long userId);
}
